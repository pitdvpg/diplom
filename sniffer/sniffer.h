#ifndef _sniffer_h
#define _sniffer_h

#include <pcap.h> 
#include <pthread.h>
#include <time.h>
#include <signal.h>
#include <stdio.h> 
#include <stdlib.h>
#include <errno.h> 
#include <sys/socket.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <netinet/if_ether.h> 
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>

struct db_flow {
 long total, ip, tcp, udp, icmp;
 long conn, pack, syn, ack, psh;
};

struct db_sys {
 float cpu, mem;
 int web, sql;
};

struct traffic {
 db_flow d[3]; //0=out, 1=in, 2=мимо
 db_sys s;
 int stored;
};

int kbhit(void);
in_addr getip(const char* interface);
void dump(int n, const u_char *data);
void tick(sigval v);
void pr(int x);
void prt(const char *s);
double getmoment(timeval tv);

float get_proc();
float get_mem();
int apache_threads();
int mysql_threads();

#endif
