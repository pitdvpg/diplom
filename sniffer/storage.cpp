#include "storage.h"
#include "sniffer.h"
#include <string.h>

#define SELECT_MAX_ID_QUERY "select max(id) from series"
#define INSERT_NEW_SERIES "insert into series(id, type, title) values (?,?,?)"
#define INSERT_NEW_DATA "insert into item(id_series, value) values (?,?)"
#define LOCK_SERIES_TABLE_QUERY "lock table series write"
#define UNLOCK_TABLES_QUERY "unlock tables"

/*
 *  Connect to database
 */
int db_connect(DBSTORAGE& db) {

    if (db.connection) {
        fprintf(stderr, "Mysql connection already established.\n");
        return 1;
    }

    mysql_init(&db.mysql);
    db.connection = mysql_real_connect(&db.mysql, db.host, db.user, db.password, db.db_name, 0, 0, 0);

    if (!db.connection) {
        fprintf(stderr, "%s", mysql_error(&db.mysql));
        return 1;
    }

    return 0;
}

/*
 *  Unlock tables
 */
int db_unlock_tables(DBSTORAGE& db) {

    MYSQL_RES* result;
    int failed;

    failed = db_request(db, &result, UNLOCK_TABLES_QUERY);
    mysql_free_result(result);
    if (failed) {
        fprintf(stderr, "Unlock tables failed.\n");
        return 1;
    }
    return 0;
}

/*
 * Store traffic data
 */
int db_store_data(DBSTORAGE& db, traffic& traf, METRIC_IDS& metric_ids) {
  
    MYSQL_STMT    *stmt;
    INSERT_NEW_DATA_DATA data;

    /* create prepared statement for insetring new data */
    stmt = db_prepare_statement(db, INSERT_NEW_DATA);
    if (!stmt) {
        return 1;
    }

    /* Bind our data object with that statement */
    if(pstmt_new_data_bind_data(stmt, data)) {
        return 1;
    }

   /* Execute all queries for storing new data */
   pstmt_execute_all_store_new_data(stmt, data, traf, metric_ids);

   /* Close the statement */
   if (mysql_stmt_close(stmt)) {
       fprintf(stderr, " failed while closing the statement\n");
       fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
       return 1;
   }
 
}

/*
 * Execute query storing metric data
 */
int pstmt_execute_store_new_data(MYSQL_STMT* stmt) {

    my_ulonglong  affected_rows;

    /* Execute the statement */
    if (mysql_stmt_execute(stmt)) {
        fprintf(stderr, " mysql_stmt_execute(), failed\n");
        fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
        return 1;
    }

    /* Get the number of affected rows */
    affected_rows = mysql_stmt_affected_rows(stmt);
    /* validate affected rows */
    if (affected_rows != 1) { 
        fprintf(stderr, " invalid affected rows by MySQL\n");
        return 1;
    }
 
    return 0;
}

/*
 *  Update series table with entries for new series
 */
int db_init_series(DBSTORAGE& db, METRIC_IDS& metric_ids) {

    MYSQL_ROW row;
    MYSQL_RES* result;
    int max_id;
    int failed;
    
    /* lock table before update -- our ids must be unique */
    failed = db_request(db, &result, LOCK_SERIES_TABLE_QUERY);
    mysql_free_result(result);
    if (failed) {
        fprintf(stderr, "Series table lock failed.\n");
        return 1;
    }

    /* Request for maximum id already in use */
    failed = db_request(db, &result, SELECT_MAX_ID_QUERY);
    if (failed) {
        fprintf(stderr, "Maximum id query failed.\n");
        mysql_free_result(result);
        db_unlock_tables(db);
        return 1;
    }

    if (mysql_num_fields(result)!=1) {
        fprintf(stderr, "Unexpected nubmer of fields in result.\n");
        mysql_free_result(result);
        db_unlock_tables(db);
        return 1;
    }

    row = mysql_fetch_row(result);
    if (row) {
        if (row[0]) {
            max_id = atoi(row[0]);
        } else {
            // empty result, no entries in series table
            max_id = 0;
        }
        mysql_free_result(result);
    } else {
        fprintf(stderr, "Series initialization failed.\n");
        mysql_free_result(result);   
        db_unlock_tables(db);
        return 1;
    }

    failed = db_create_new_series(db, max_id+1, metric_ids);
    if (failed) {
        fprintf(stderr, "db_prepared() failed.\n");
        db_unlock_tables(db);
        return 1;
    }

    failed = db_unlock_tables(db);
    return failed;
}

/*
 *  Create prepared statement
 */
MYSQL_STMT* db_prepare_statement(DBSTORAGE& db, const char* stmt_string) {

    MYSQL_STMT* stmt;

    stmt = mysql_stmt_init(db.connection);

    if (!stmt) {
        fprintf(stderr, " mysql_stmt_init(), out of memory\n");
        return NULL;
    }

    if (mysql_stmt_prepare(stmt, stmt_string, strlen(stmt_string))) {
        fprintf(stderr, " mysql_stmt_prepare(), INSERT failed\n");
        fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
        return NULL;
    }    
    return stmt;
}

/*
 * Bind data to INSERT_NEW_DATA prepared statement 
 */
int pstmt_new_data_bind_data(MYSQL_STMT* stmt, INSERT_NEW_DATA_DATA& data) {

    int param_count;

    /* validate parameter count */
    param_count = mysql_stmt_param_count(stmt);
    if (param_count != 2) { 
        fprintf(stderr, " invalid parameter count returned by MySQL\n");
        return 1;
    }

    /* Bind the data for all 3 parameters */
    memset(data.bind, 0, sizeof(data.bind));

    /* id_series */
    /* This is a number type, so there is no need to specify buffer_length */
    data.bind[0].buffer_type = MYSQL_TYPE_LONG;
    data.bind[0].buffer = (char *)&data.id_series;
    data.bind[0].is_null = 0;
    data.bind[0].length = 0;

    /* value */
    /* This is a number type, so there is no need to specify buffer_length */
    data.bind[1].buffer_type = MYSQL_TYPE_LONG;
    data.bind[1].buffer = (char *)&data.value;
    data.bind[1].is_null = 0;
    data.bind[1].length = 0;

    /* Bind the buffers */
    if (mysql_stmt_bind_param(stmt, data.bind)) {
        fprintf(stderr, " mysql_stmt_bind_param() failed\n");
        fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
        return 1;
    }
}

/*
 * Bind data to INSERT_NEW_DATA prepared statement 
 */
int pstmt_new_series_bind_data(MYSQL_STMT* stmt, INSERT_NEW_SERIES_DATA& data) {

    int param_count;

    /* validate parameter count */
    param_count = mysql_stmt_param_count(stmt);
    if (param_count != 3) { 
        fprintf(stderr, " invalid parameter count returned by MySQL\n");
        return 1;
    }

    /* Bind the data for all 3 parameters */
    memset(data.bind, 0, sizeof(data.bind));

    /* id */
    /* This is a number type, so there is no need to specify buffer_length */
    data.bind[0].buffer_type = MYSQL_TYPE_LONG;
    data.bind[0].buffer = (char *)&data.id;
    data.bind[0].is_null = 0;
    data.bind[0].length = 0;

    /* type */
    /* This is a number type, so there is no need to specify buffer_length */
    data.bind[1].buffer_type = MYSQL_TYPE_LONG;
    data.bind[1].buffer = (char *)&data.type;
    data.bind[1].is_null = 0;
    data.bind[1].length = 0;

    /* title */
    data.bind[2].buffer_type = MYSQL_TYPE_STRING;
    data.bind[2].buffer = (char *)data.title;
    data.bind[2].buffer_length = TITLE_STRING_MAX_SIZE;
    data.bind[2].is_null = 0;
    data.bind[2].length = &data.str_length;

    /* Bind the buffers */
    if (mysql_stmt_bind_param(stmt, data.bind)) {
        fprintf(stderr, " mysql_stmt_bind_param() failed\n");
        fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
        return 1;
    }
}

/*
 * Set the data for INSERT_NEW_SERIES prepared statement
 */
void pstmt_new_series_set_data(INSERT_NEW_SERIES_DATA& data, int id, int type, const char* title) {
    data.id = id; 
    data.type = type;             
    strncpy(data.title, title, TITLE_STRING_MAX_SIZE);    
    data.str_length = strlen(data.title);
}

/*
 * Update series table with entries for new series.
 * start_id - starting id for series. it must be unique.
 */
int db_create_new_series(DBSTORAGE& db, int start_id, METRIC_IDS& metric_ids) {
    
    MYSQL_STMT    *stmt;
    INSERT_NEW_SERIES_DATA data;
    my_ulonglong  affected_rows;

    /* create prepared statement for inserting data about new series */
    stmt = db_prepare_statement(db, INSERT_NEW_SERIES);
    if (!stmt) {
        return 1;
    }
    
    /* Bind our data object with that statement */
    if(pstmt_new_series_bind_data(stmt, data)) {
        return 1;
    }

    /* Titles for new series */
    char titles[18][31] = { "Total incoming traffic volume", 
                            "Incoming IP traffic volume", 
                            "Incoming TCP traffic volume", 
                            "Incoming UDP traffic volume", 
                            "Incoming ICMP traffic volume", 
                            "Number of incoming packets", 
                            "Number of incoming SYN-flags", 
                            "Number of incoming ACK-flags", 
                            "Number of incoming PSH-flags",
                            "Total outcoming traffic volume", 
                            "Outcoming IP traffic volume", 
                            "Outcoming TCP traffic volume", 
                            "Outcoming UDP traffic volume", 
                            "Outcoming ICMP traffic volume", 
                            "Number of outcoming packets", 
                            "Number of outcoming SYN-flags", 
                            "Number of outcoming ACK-flags", 
                            "Number of outcoming PSH-flags"};

    /* Titles for new series */
    for (int i = 0; i < 18; i++) {
        /* Store id in the metric_ids structure */
        int id = start_id+i;
        ((int*)&metric_ids)[i]=id;
        /* Set id, type and title for new series */
        pstmt_new_series_set_data(data, id, 2, titles[i]);

        /* Execute the statement */
        if (mysql_stmt_execute(stmt)) {
            fprintf(stderr, " mysql_stmt_execute(), failed\n");
            fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
            return 1;
        }
    
        /* Get the number of affected rows */
        affected_rows = mysql_stmt_affected_rows(stmt);
        /* validate affected rows */
        if (affected_rows != 1) { 
            fprintf(stderr, " invalid affected rows by MySQL\n");
            return 1;
        }
    }

   /* Close the statement */
   if (mysql_stmt_close(stmt)) {
       fprintf(stderr, " failed while closing the statement\n");
       fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
       return 1;
   }

}

/*
 *  Do request to the database
 *  
 *  db - database handle
 *  result - place for request result
 *  request - query string
 */
int db_request(DBSTORAGE& db, MYSQL_RES** result, const char* request) {

    int state;    

    if (!db.connection) {
        fprintf(stderr, "Mysql connection not found.\n");
        return 1;
    }

    if (mysql_query(db.connection, request)) {
        fprintf(stderr, "%s", mysql_error(db.connection));
        return 1;
    }

    *result = mysql_store_result(db.connection);
    return 0;
}

/*
 * Close database connection
 */
int db_close(DBSTORAGE& db) {

    if (!db.connection) {
        fprintf(stderr, "Mysql connection not found.\n");
        return 1;
    }

    mysql_close(db.connection);
    db.connection = NULL;
}

int pstmt_execute_all_store_new_data(MYSQL_STMT* stmt, INSERT_NEW_DATA_DATA& data, traffic& traf, METRIC_IDS& metric_ids) {
    // incoming /////////////////

    // суммарный объём трафика
    data.value = traf.d[1].total;
    data.id_series = metric_ids.in_total_id;
    pstmt_execute_store_new_data(stmt);    

    // объём IP-трафика
    data.value = traf.d[1].ip;
    data.id_series = metric_ids.in_ip_id;
    pstmt_execute_store_new_data(stmt);    

    // объём TCP-трафика
    data.value = traf.d[1].tcp;
    data.id_series = metric_ids.in_tcp_id;
    pstmt_execute_store_new_data(stmt);    

    // объём UDP-трафика
    data.value = traf.d[1].udp;
    data.id_series = metric_ids.in_udp_id;
    pstmt_execute_store_new_data(stmt);    

    // объём ICMP-трафика
    data.value = traf.d[1].icmp;
    data.id_series = metric_ids.in_icmp_id;
    pstmt_execute_store_new_data(stmt);    

    // число пакетов
    data.value = traf.d[1].pack;
    data.id_series = metric_ids.in_pack_id;
    pstmt_execute_store_new_data(stmt);    

    // число SYN-флагов
    data.value = traf.d[1].syn;
    data.id_series = metric_ids.in_syn_id;
    pstmt_execute_store_new_data(stmt);    

    // число ACK-флагов
    data.value = traf.d[1].ack;
    data.id_series = metric_ids.in_ack_id;
    pstmt_execute_store_new_data(stmt);    

    // число PSH-флагов
    data.value = traf.d[1].psh;
    data.id_series = metric_ids.in_psh_id;
    pstmt_execute_store_new_data(stmt);    

    // outcoming ////////////

    // суммарный объём трафика
    data.value = traf.d[0].total;
    data.id_series = metric_ids.out_total_id;
    pstmt_execute_store_new_data(stmt);    

    // объём IP-трафика
    data.value = traf.d[0].ip;
    data.id_series = metric_ids.out_ip_id;
    pstmt_execute_store_new_data(stmt);    

    // объём TCP-трафика
    data.value = traf.d[0].tcp;
    data.id_series = metric_ids.out_tcp_id;
    pstmt_execute_store_new_data(stmt);    

    // объём UDP-трафика
    data.value = traf.d[0].udp;
    data.id_series = metric_ids.out_udp_id;
    pstmt_execute_store_new_data(stmt);    

    // объём ICMP-трафика
    data.value = traf.d[0].icmp;
    data.id_series = metric_ids.out_icmp_id;
    pstmt_execute_store_new_data(stmt);    

    // число пакетов
    data.value = traf.d[0].pack;
    data.id_series = metric_ids.out_pack_id;
    pstmt_execute_store_new_data(stmt);    

    // число SYN-флагов
    data.value = traf.d[0].syn;
    data.id_series = metric_ids.out_syn_id;
    pstmt_execute_store_new_data(stmt);    

    // число ACK-флагов
    data.value = traf.d[0].ack;
    data.id_series = metric_ids.out_ack_id;
    pstmt_execute_store_new_data(stmt);    

    // число PSH-флагов
    data.value = traf.d[0].psh;
    data.id_series = metric_ids.out_psh_id;
    pstmt_execute_store_new_data(stmt);    
}
