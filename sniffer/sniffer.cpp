#include "sniffer.h"
#include "storage.h"

#define DB_HOST "localhost"
#define DB_NAME "forecast"

pcap_t* descr;
in_addr myip;
long timebase = -1;
double period = 1.0;
bool silent = true, finish = false;

const long periods = 3600;
long maxper = periods;
traffic db[periods];

DBSTORAGE storage; 
METRIC_IDS metric_ids;

void save_results() {
 time_t rawtime; 
 struct tm * timeinfo; 
 
 time (&rawtime); 
 timeinfo = localtime(&rawtime);
 char *tstr = asctime(timeinfo);
 char dir[50];
 long i, j;
 for (i = 4; i < 20; i++) {
  if (tstr[i] == ':')
   dir[i - 4] = '_';
  else
   dir[i - 4] = tstr[i];
 }
 dir[i - 4] = 0;
 printf ("Сохранение данных в папку %s\n", dir);  
 int status = mkdir(dir, 0777);
 if (status != 0) {
  printf("Failed to create directory.\n");
  return;
 }

 chdir(dir);
 const char* files[] = {"ip_i.txt", "icmp_i.txt", "tcp_i.txt", "udp_i.txt", "pack_i.txt", "conn_i.txt", 
	"syn_i.txt", "ack_i.txt", "psh_i.txt", "ip_o.txt", "icmp_o.txt", "tcp_o.txt", "udp_o.txt", 
	"pack_o.txt", "conn_o.txt", "syn_o.txt", "ack_o.txt", "psh_o.txt", "syscpu.txt", "sysram.txt",
	"sysweb.txt", "syssql.txt"};
 umask(0);
 FILE *f;
 for (i = 0; i < 22; i++) {
  f = fopen(files[i], "w");
  for (j = 0; j < maxper; j++) {
   switch(i) {
	// входящий
    case 0: fprintf(f, "%li\n", db[j].d[1].ip); break;
    case 1: fprintf(f, "%li\n", db[j].d[1].icmp); break;
    case 2: fprintf(f, "%li\n", db[j].d[1].tcp); break;
    case 3: fprintf(f, "%li\n", db[j].d[1].udp); break;
    case 4: fprintf(f, "%li\n", db[j].d[1].pack); break;
    case 5: fprintf(f, "%li\n", db[j].d[1].conn); break;
    case 6: fprintf(f, "%li\n", db[j].d[1].syn); break;
    case 7: fprintf(f, "%li\n", db[j].d[1].ack); break;
    case 8: fprintf(f, "%li\n", db[j].d[1].psh); break;
	// исходящий
    case 9: fprintf(f, "%li\n", db[j].d[0].ip); break;
    case 10:fprintf(f, "%li\n", db[j].d[0].icmp); break;
    case 11:fprintf(f, "%li\n", db[j].d[0].tcp); break;
    case 12:fprintf(f, "%li\n", db[j].d[0].udp); break;
    case 13:fprintf(f, "%li\n", db[j].d[0].pack); break;
    case 14:fprintf(f, "%li\n", db[j].d[0].conn); break;
    case 15:fprintf(f, "%li\n", db[j].d[0].syn); break;
    case 16:fprintf(f, "%li\n", db[j].d[0].ack); break;
    case 17:fprintf(f, "%li\n", db[j].d[0].psh); break;
	// система
    case 18:fprintf(f, "%f\n", db[j].s.cpu); break;
    case 19:fprintf(f, "%f\n", db[j].s.mem); break;
    case 20:fprintf(f, "%i\n", db[j].s.web); break;
    case 21:fprintf(f, "%i\n", db[j].s.sql); break;
    default: break;
   };
  }
  fclose(f);
 }
 chdir("..");
 printf("\n%s\n", "##### D ______IP _ICMP __TCP __UDP  NPACK NCONN _NSYN _NACK _NPSH CPU/MEM WEB/SQL");
 for (i = 0; i < maxper; i++) {
  printf("%5li i %8li %5li %5li %5li  %5li %5li %5li %5li %5li %5.1f %2d\n", i, db[i].d[1].ip, db[i].d[1].icmp, db[i].d[1].tcp,
	db[i].d[1].udp, db[i].d[1].pack, db[i].d[1].conn, db[i].d[1].syn, db[i].d[1].ack, db[i].d[1].psh, db[i].s.cpu, db[i].s.web);
  printf("      o %8li %5li %5li %5li  %5li %5li %5li %5li %5li %5.2f %2d\n", db[i].d[0].ip, db[i].d[0].icmp, db[i].d[0].tcp,
	db[i].d[0].udp, db[i].d[0].pack, db[i].d[0].conn, db[i].d[0].syn, db[i].d[0].ack, db[i].d[0].psh, db[i].s.mem, db[i].s.sql);
 }
}

void parse_ip(int cl, const u_char* p, int np) {
//	dump(cl, p);
	int i;
	int hdrlen = int(p[14]) % 16 * 4;
	int pcklen = int(p[17]) + int(p[16]) * 256;
	int prot = int(p[23]);
	in_addr src, dst;
	src.s_addr = p[26] + 256*p[27] + 65536*p[28] + 256*65536*p[29];
	dst.s_addr = p[30] + 256*p[31] + 65536*p[32] + 256*65536*p[33];
	int direction, base, flags;
	long sn, an;
	char flg[7];
	flg[6] = 0;
	if (src.s_addr == myip.s_addr)
		direction = 0;
	else if (dst.s_addr == myip.s_addr)
		direction = 1;
	else
		direction = 2;
	if (!silent) {
	 printf("Source: %s\n", inet_ntoa(src));
	 printf("Dest. : %s\n", inet_ntoa(dst));
	 printf("Prot. : %d\n", prot);
	 printf("Direction: %s\n", direction==0?"out":(direction==1?"in":"?"));
	}
	db[np].d[direction].total += cl;
	db[np].d[direction].pack ++;
	db[np].d[direction].ip += cl;
	switch(prot) {
	case 1: prt("ICMP"); 
		db[np].d[direction].icmp += cl;
		break;
	case 4: prt("IP4"); 
		break;
	case 6: prt("TCP\n"); 
		db[np].d[direction].tcp += cl;
		base = hdrlen + 14;
		sn = p[base+7] + 256*p[base+6] + 65536*p[base+5] + 256*65536*p[base+4];
		an = p[base+11] + 256*p[base+10] + 65536*p[base+9] + 256*65536*p[base+8];
		flags = p[base + 13] & 0x3F;
		flg[0] = (flags & 1) ? 'F' : '.';
		flg[1] = (flags & 2) ? 'S' : '.';
		flg[2] = (flags & 4) ? 'R' : '.';
		flg[3] = (flags & 8) ? 'P' : '.';
		flg[4] = (flags & 16) ? 'A' : '.';
		flg[5] = (flags & 32) ? 'U' : '.';
		if (flags & 2) db[np].d[direction].syn ++;
		if (flags & 8) db[np].d[direction].psh ++;
		if (flags & 16) db[np].d[direction].ack ++;
//		if (flags == 16)
		if (!silent) {
		 fprintf(stdout, "SN: %li AN: %li\n", sn, an);
		 fprintf(stdout, "Flags: [%s]\n", flg);
		}
		break;
	case 17: prt("UDP"); 
		db[np].d[direction].udp += cl;
		break;
	case 41: prt("IP6"); break;
	case 58: prt("IP6-ICMP"); break;
	default: prt("???"); break;
	};

}

void my_callback(u_char *args, const struct pcap_pkthdr* pkthdr, const u_char* packet) 
{ 
	long i;
	static int count = 1; 
	int cl = pkthdr->caplen, l = pkthdr->len;
	double moment = getmoment(pkthdr->ts);
	if (timebase == -1) timebase = long(moment);
	long nper = (long(moment) - timebase) / long(period);
	printf("Working %li\n", nper);
	if (nper<periods && nper>0 && db[nper-1].stored == 0)  {
          // Сбросить данные в базу
          db_store_data(storage, db[nper-1], metric_ids);
          // Пометить период как сохранённый
	  db[nper-1].stored = 1;
        }

	timeval tv;
	gettimeofday(&tv, 0);
//  	printf("%li ----------\n", nper);

	if (finish) {
	 pcap_breakloop(descr);
	 return;
	}

	if (!silent) {
	 fprintf(stdout, "#%d\t", count);
//	 fprintf(stdout, "%li %li \n", pkthdr->ts.tv_sec, pkthdr->ts.tv_usec);
	 fprintf(stdout, "at %10.3f, len %d/%d, period %li\n", moment, cl, l, nper);
	}

// структура!!!
// 14 байт - кон. mac 6, исх. mac 6, длина-тип 2, данные начинаются у ip с байта 69, до него 0
	int ethertype = int(packet[12]) * 256 + int(packet[13]);
	if (ethertype == 0x0800) {
//		fprintf(stdout, "IP4 ");
		parse_ip(cl, packet, nper);
//		dump(cl, packet);
	}
	else if (ethertype == 0x86DD) {
//		fprintf(stdout, "IP6 \n");
//		dump(cl, packet);
	}
	else
		fprintf(stdout, "Unknown ethertype %x\n", ethertype);
	fflush(stdout);
	count++; 
}

void *system_watcher(void* pcd) {
 long i, moment;
 timeval tv;

 printf("CPU/RAM watcher is ON.\n");
 while(true) {
  gettimeofday(&tv, 0);
  moment = long(getmoment(tv));
  i = (moment - timebase) / long(period);
  if (db[i].s.mem == 0) {
   db[i].s.cpu = get_proc();
   db[i].s.mem = get_mem();
   db[i].s.web = apache_threads();
   db[i].s.sql = mysql_threads();
  }
//  printf("%li %f %f\n", i, db[i].s.cpu, db[i].s.mem);
  char c = kbhit();
  if (c == 'q') {
   printf("Quit\n");
   finish = true;
   maxper = i;
   break;
  }
  if (i >= periods) {
   printf("End\n");
   finish = true;
   maxper = i;
   break;
  }
  if (finish) break;
  usleep(period*500000.0);
 }
 printf("CPU/RAM watcher is OFF.\n");
}

int main(int argc,char **argv) 
{ 
	long i, j;
	char *dev;
	char errbuf[PCAP_ERRBUF_SIZE]; 
	const u_char *packet; 
	struct pcap_pkthdr hdr;
	struct ether_header *eptr; 
	struct bpf_program fp;     
	bpf_u_int32 maskp;         
	bpf_u_int32 netp;         
	for (i = 0; i < periods; i++) {
	 for (j = 0; j < 3; j++) {
 	  db[i].d[j].total = 0;
 	  db[i].d[j].ip = 0;
 	  db[i].d[j].tcp = 0;
 	  db[i].d[j].udp = 0;
 	  db[i].d[j].icmp = 0;
 	  db[i].d[j].conn = 0;
 	  db[i].d[j].pack = 0;
	  db[i].d[j].syn = 0;
	  db[i].d[j].ack = 0;
	  db[i].d[j].psh = 0;
	 }
	 db[i].s.cpu = 0;
	 db[i].s.mem = 0;
	 db[i].s.web = 0;
	 db[i].s.sql = 0;
         db[i].stored = 0;
	}

	if(argc != 4){
		fprintf(stdout, "Usage: %s device dbuser dbpassword\n" 
			,argv[0]);
		return 0;
	} 


	/* доступ к БД */
	storage.user = argv[2];
	storage.password = argv[3];
	storage.host = DB_HOST;
        storage.db_name = DB_NAME;
	if (db_connect(storage)) {
	    fprintf(stdout, "Database connection failed.\n");
	    return 1;
	}

/*	dev = pcap_lookupdev(errbuf); 
	
	if(dev == NULL) {
		fprintf(stderr, "%s\n", errbuf);
		exit(1);
	}*/
	

	if (db_init_series(storage, metric_ids)) {
	    fprintf(stdout, "Series initialization failed.\n");
	    return 1;
	}

	dev = argv[1];
	printf("Interface: %s\n", dev);
	myip = getip(dev);
	char *ipstr = inet_ntoa(myip);
	printf("Address: %s\n", ipstr);
	uint32_t a = myip.s_addr;
	/* Получение сетевого адреса и маски сети для устройства */ 
	pcap_lookupnet(dev, &netp, &maskp, errbuf); 

	/* открытие устройства*/ 
	descr = pcap_open_live(dev, BUFSIZ, 1,-1, errbuf); 
	if(descr == NULL) {
		printf("pcap_open_live(): %s\n", errbuf);
		exit(1);
	} 

	char* filter = new char[40];
	filter[0] = 0;
//	strcat(filter, "host 192.168.29.34 and host ");
	strcat(filter, "host ");
	strcat(filter, ipstr);
	if(pcap_compile(descr, &fp, filter, 0, netp) == -1) {
		fprintf(stderr, "Error calling pcap_compile\n");
		exit(1);
	} 

	if(pcap_setfilter(descr, &fp) == -1) {
		fprintf(stderr, "Error setting filter\n");
		exit(1);
	} 

	timeval(tv);
	gettimeofday(&tv, 0);
	timebase = long(getmoment(tv));

	pthread_attr_t threadAttr; 
 	pthread_attr_init(&threadAttr);
	pthread_t watcher;
	int result = pthread_create(&watcher, &threadAttr, system_watcher, (void*)descr);
 
	if (result != 0) {
		printf("Can't launch system_watcher. Error %d\n", result);
		return 0;
	}

	printf("Sniffer is ON.\n");
	pcap_loop(descr, -1, my_callback, NULL); 
	printf("Sniffer is OFF.\n");
	pcap_close(descr);
	db_close(storage);
//	finish = true;
//	pthread_join(watcher, NULL);
	save_results();
	return 0; 
}

