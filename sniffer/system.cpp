#include <iostream>
#include <stdio.h>
#include <time.h>
#include <sys/sysinfo.h>

float get_proc() {
 float res;
 FILE *avgload = fopen("/proc/loadavg", "rt");
 fscanf(avgload, "%5f", &res);
 fclose(avgload);
 return res;
}

float get_mem() {
 struct sysinfo si;
 sysinfo(&si);
 double total = (double)si.totalram*(double)si.mem_unit/1048576;
 double free = (double)si.freeram*(double)si.mem_unit/1048576;
 float used = 1.0 - free / total;
 return used;
}

int apache_threads() {
 FILE *f = popen("ps -e | grep apache2$ | wc -l", "r");
 int n;
 fscanf(f, "%d", &n);
 pclose(f);
 return n;
}

int mysql_threads() {
 FILE *f = popen("ps -e | grep mysqld$ | wc -l", "r");
 int n;
 fscanf(f, "%d", &n);
 pclose(f);
 return n;
}

