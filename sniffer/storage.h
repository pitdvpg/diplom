#ifndef _storage_h
#define _storage_h

#include "sniffer.h"
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <mysql.h>

typedef struct {
    const char* user;
    const char* password;
    const char* host;
    const char* db_name;
    MYSQL*      connection;
    MYSQL       mysql;
} DBSTORAGE;

#define TITLE_STRING_MAX_SIZE 256

typedef struct {
    MYSQL_BIND    bind[3];
    char          title[TITLE_STRING_MAX_SIZE];
    unsigned long str_length;
    int           id;
    int           type;
} INSERT_NEW_SERIES_DATA;

typedef struct {
    MYSQL_BIND    bind[2];
    int           id_series;
    int           value;
} INSERT_NEW_DATA_DATA;

/* id для метрик. Заполняется как int[], так что порядок и тип лучше не менять.*/
typedef struct {
// incoming
    int in_total_id; // суммарный объём трафика
    int in_ip_id; // объём IP-трафика
    int in_tcp_id; // объём TCP-трафика
    int in_udp_id; // объём UDP-трафика
    int in_icmp_id; // объём ICMP-трафика
    int in_pack_id; // число пакетов
    int in_syn_id; // число SYN-флагов
    int in_ack_id; // число ACK-флагов
    int in_psh_id; // число PSH-флагов
// outcoming
    int out_total_id; // суммарный объём трафика
    int out_ip_id; // объём IP-трафика
    int out_tcp_id; // объём TCP-трафика
    int out_udp_id; // объём UDP-трафика
    int out_icmp_id; // объём ICMP-трафика
    int out_pack_id; // число пакетов
    int out_syn_id; // число SYN-флагов
    int out_ack_id; // число ACK-флагов
    int out_psh_id; // число PSH-флагов
} METRIC_IDS;

int db_connect(DBSTORAGE& db);
int db_store_data(DBSTORAGE& db, traffic& traf, METRIC_IDS& metric_ids);
int pstmt_execute_store_new_data(MYSQL_STMT* stmt);
int db_init_series(DBSTORAGE& db, METRIC_IDS& metric_ids);
MYSQL_STMT* db_prepare_statement(DBSTORAGE& db, const char* stmt_string);
int pstmt_new_data_bind_data(MYSQL_STMT* stmt, INSERT_NEW_DATA_DATA& data);
int db_create_new_series(DBSTORAGE& db, int start_id, METRIC_IDS& metric_ids);
int db_request(DBSTORAGE& db, MYSQL_RES** result, const char* request);
int db_close(DBSTORAGE& db);
int pstmt_execute_all_store_new_data(MYSQL_STMT* stmt, INSERT_NEW_DATA_DATA& data, traffic& traf, METRIC_IDS& metric_ids);
#endif