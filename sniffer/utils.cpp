#include "sniffer.h"

double getmoment(timeval tv) {
 return double(tv.tv_sec) + double(tv.tv_usec) / 1000000.0;
}

int kbhit(void) 
{ 
 int fd; char c; 
 fd = fileno(stdin); 
 fcntl(fd, F_SETFL, O_NONBLOCK); 
 if (read(fd, &c, sizeof(char)) < 0) return 0; 
 return c; 
}

in_addr getip(const char* interface) {
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));
    strcpy(ifr.ifr_name, interface);

    int s = socket(AF_INET, SOCK_DGRAM, 0);
    ioctl(s, SIOCGIFADDR, &ifr);
    close(s);

    struct sockaddr_in *sa = (struct sockaddr_in*)&ifr.ifr_addr;
    return sa->sin_addr;
}

void dump(int n, const u_char *data) {
	for (int i = 0; i < n; i++) {
		fprintf(stdout, "%x ", int(data[i]));
		if (i % 16 == 15) fprintf(stdout, "\n");
	}
	fprintf(stdout, "\n");
}

void tick(sigval v) {
 printf("Tick\n");
}

void pr(int x) {
//	if (silent) return;
//	fprintf(stdout, "=%d\n", x);
}

void prt(const char *s) {
//	if (silent) return;
//	fprintf(stdout, "%s", s);
}


