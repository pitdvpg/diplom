$(function(){
    var renameSeriesTitle = $("#rename-series-title");
    var renameSeriesId = $('#rename-series-id');

    tips = $( ".validateTips" );
    function updateTips( t ) {
        tips.text( t ).addClass( "ui-state-highlight" );
        setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 1500 );
        }, 500 );
    }

    function checkLength( o, max ) {
        if ( o.val().length > max || o.val().length < 1 ) {
            o.addClass( "ui-state-error" );
            updateTips( "Строка не должна быть пустой или более " + max + " символов.");
            return false;
        } else {
            return true;
        }
    }

    dialogRenameSeries = $( "#rename-series-dialog" ).dialog({
        autoOpen: false,
        height: 250,
        width: 400,
        modal: true,
        buttons: {
            "Переименовать": renameSeries,
            "Отмена": function() {
                dialogRenameSeries.dialog( 'close' );
            }
        },
        close: function() {
            form[ 0 ].reset();
            renameSeriesTitle.removeClass( 'ui-state-error' );
        }
    });

    form = dialogRenameSeries.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
        renameSeries();
    });

    var seriesMenu = $('#series');
    seriesMenu.selectmenu({
        width: 400,
        change: onChangeSeriesData
    });

    $('#renameSeries').button({
        icons: {
            primary: 'ui-icon-pencil'
        },
        text: false
    }).click(function() {
        var selected = seriesMenu.find(':selected');
        var id = selected.val();
        var title = selected.html();
        tips.text("Здесь можно изменить название серии.");
        renameSeriesId.val(id);
        renameSeriesTitle.val(title);
        dialogRenameSeries.dialog('open');
    });

    function renameSeries() {
        var valid = true;
        valid = valid && checkLength( renameSeriesTitle, 255 );
        if (valid) {
            // do something
            var id = renameSeriesId.val();
            var title = renameSeriesTitle.val();

            var selected = seriesMenu.find(':selected');
            selected.html(title);
            seriesMenu.selectmenu('refresh');

            $.ajax({
                dataType: 'json',
                url:      'setSeries',
                data:     { id:renameSeriesId.val(), title:renameSeriesTitle.val()},
                success:  function(result) {
                    dialogRenameSeries.dialog('close');
                }
            });

            tips.text( "Сохранение..." );
        }
    }

    $.ajax({
        dataType: 'json',
        url:      'getPages',
        success:  onLoadNumberOfPages
    });
});


function onLoadNumberOfPages(pagesData) {
    spinner = $('#page');
    spinner.spinner({
        min:  1,
        max: pagesData.max,
        spin: onPageSpin
    });
    spinner.spinner( "value", 1 );
    $.ajax({
        dataType: 'json',
        url:      'getSeries?page=1',
        success:  onLoadSeriesData
    });
}

function onPageSpin(event, ui) {
    loadSeriesData(ui.value);
}

function loadSeriesData(value) {
    $.ajax({
        dataType: 'json',
        url:      'getSeries?page=' + value,
        success:  onLoadSeriesData
    });
}

function onLoadSeriesData(seriesData) {
    var select = $('#series');
    select.empty();
    if (seriesData.length > 0) {
        $.each(seriesData, function (i, val) {
            select.append($('<option />', {value: val.id, text: val.title, selected: i==0}));
        });
        var val = select.find(':selected').val();
        select.selectmenu( 'enable' );
        select.selectmenu( 'refresh' );
        loadItemsData(val);
    } else {
        select.selectmenu( 'disable' );
        select.selectmenu( 'refresh' );
        // Данные по сериям закончились, ограничиваем спиннер текущим значением.
        var page = $('#page');
        var value = page.spinner('value');
        page.spinner( 'option', 'max', value );
    }
}

function onChangeSeriesData(event, ui) {
    loadItemsData(ui.item.value);
}

function loadItemsData(series) {
    $.ajax({
        dataType: 'json',
        url: 'getItems?series='+series,
        success: function (itemData) {
            drawChart(itemData);
        }
    });
}

function drawChart(jsondata) {
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'X');
    data.addColumn('number', 'Величина');

    var i;
    var rows = [];
    for (i = 0; i < jsondata.length; i++) {
        rows[i] = [i, jsondata[i].value];
    }
    data.addRows(rows);

    var options = {
        width: 1000,
        height: 563,
        hAxis: {
            title: 'Время'
        },
        vAxis: {
            title: 'Количество'
        }
    };

    var chart = new google.visualization.LineChart(
        document.getElementById('chart'));

    chart.draw(data, options);
}