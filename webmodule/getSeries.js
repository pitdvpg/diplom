
var mysql = require('mysql2');
var mime = require('mime');

var http = require('http');
var url = require('url');
var path = require('path');
var util = require('util');
var fs = require('fs');

var settings = require('./settings');

var ROOT = path.normalize(__dirname + "/html/");

var pool = mysql.createPool({
  host: settings['db_host'],
  user: settings['db_user'],
  password: settings['db_password'],
  database: settings['db_name']
});

var server = new http.Server();
server.listen(settings['http_port']);

server.on('request', function(req, res) {
    var urlParsed = url.parse(req.url, true);
    if (urlParsed.pathname == '/getSeries' && urlParsed.query.page) {
      processGetSeries(urlParsed, res, req);
    } else if (urlParsed.pathname == '/setSeries') {
      processSetSeries(urlParsed, res, req);
    } else if (urlParsed.pathname == '/getPages') {
      processGetPages(urlParsed, res);
    } else if (urlParsed.pathname == '/getItems') {
      processGetItems(urlParsed, res, req);
    } else {
      sendFileSafe(url.parse(req.url).pathname, res);
    }
});

function processGetPages(urlParsed, res) {
  // getPages
  pool.query('select max(id) as max from series', function(err, rows, fields) {
    if (err) {
      sendError(res, 500, err);
      return;
    }
    var maxPage = Math.ceil(rows[0].max / 18);
    var json = JSON.stringify({max:maxPage});
    res.end(json);
  });
}

function processGetSeries(urlParsed, res, req) {
  // getSeries?page=1
  var page = parseInt(urlParsed.query.page, 10);
  if (isNaN(page)) {
    sendError(res, 400, req.url);
    return;
  }
  var minId = (page-1)*18+1;
  var maxId = (page)*18;
  pool.query('select id, title from series where id>=? and id<=?', [minId, maxId], function(err, rows, fields) {
    if (err) {
      sendError(res, 500, err);
      return;
    }
    var json = JSON.stringify(rows);
    res.end(json);
  });
}

function processSetSeries(urlParsed, res, req) {
  // setSeries?id=1&title=XXX
  var id = parseInt(urlParsed.query.id, 10);
  var title = urlParsed.query.title;
  if (isNaN(id) || !title) {
    sendError(res, 400, req.url);
    return;
  }
  pool.query('update series set title=? where id=?', [title, id], function(err, result) {
    if (err) {
      sendError(res, 500, err);
      return;
    }
    var json = JSON.stringify({ok:true});
    res.end(json);
  });
}

function processGetItems(urlParsed, res, req) {
  // getItems?series=1
  var series_id = parseInt(urlParsed.query.series, 10);
  if (isNaN(series_id)) {
    sendError(res, 400, req.url);
    return;
  }
  pool.query('select id_series, value from item where id_series=?', [series_id], function(err, rows, fields) {
    if (err) {
      sendError(res, 500, err);
      return;
    }
    var json = JSON.stringify(rows);
    res.end(json);
  });
}

function sendFileSafe(filePath, res) {
  // Декодирование URL и подстановка index.html
  try {
    filePath = decodeURIComponent(filePath);
    if (filePath == '/') {
      filePath = '/index.html';
    }
  } catch(e) {
    sendError(res, 400);
    return;
  }

  // Кто-то задумал что-то нехорошее...
  if (~filePath.indexOf('\0')) {
    sendError(res, 400);
    return;
  }

  // Получаем абсолютный путь к файлу
  filePath = path.normalize(path.join(ROOT, filePath));

  // Убедимся, что не вышли за пределы нашего веб-рута
  if (filePath.indexOf(ROOT) != 0) {
    serError(res, 404);
    return;
  }

  // Убедимся, что файл существует.
  fs.stat(filePath, function(err, stats) {
    if (err || !stats.isFile()) {
      sendError(res, 404);
      return;
    }

    sendFile(filePath, res);
  });
}

function sendFile(filePath, res) {
  var file = new fs.ReadStream(filePath);
  file.on('error', function(err) {
    if (err.code == 'ENOENT') {
      sendError(res, 404);
    } else {
      sendError(res, 500, err);
    }
  });
  res.on('close', function() {
    file.destroy();
  });

  res.setHeader('Content-Type', mime.lookup(filePath) + "; charset=utf-8");
  file.pipe(res);
}

function sendError(res, code, err) {
  var message;
  switch (code) {
    case 500: message = 'На сервере произошла ошибка!\n'; break;
    case 400: message = 'Ошибка запроса.\n'; break;
    case 404: message = 'Страница не найдена.\n'; break;
    default : message = 'Ошибка с кодом ' + code + '.\n';
  }
  if (err) {
    console.error(err);
  }
  res.statusCode = code;
  res.end(message + (err?util.inspect(err):''));
}